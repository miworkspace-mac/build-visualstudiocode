#!/bin/bash


NEWLOC=`curl -I -L "https://code.visualstudio.com/sha/download?build=stable&os=darwin-universal" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 Safari/605.1.15' 2>/dev/null | grep '^location' | sed 's/location: //' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
